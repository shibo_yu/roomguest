json.array!(@rooms) do |room|
  json.extract! room, :id, :name, :address, :capacity
  json.url room_url(room, format: :json)
end
