class Guest < ActiveRecord::Base
	validates :name, case_sensitive: false, uniqueness: true, presence: true

	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create },
										presence: true, uniqueness: true, case_sensitive: false
	validates :room_id, presence: true, numericality: { only_integer: true}

	belongs_to :room
end
