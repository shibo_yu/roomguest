class GuestsController < ApplicationController
  before_action :set_guest, only: [:show, :edit, :update, :destroy]

  # GET /guests
  # GET /guests.json
  def index
    @guests = Guest.all
  end

  # GET /guests/1
  # GET /guests/1.json
  def show
  end

  # GET /guests/new
  def new
    @guest = Guest.new
  end

  # GET /guests/1/edit
  def edit
  end

  # POST /guests
  # POST /guests.json
  def create
    @guest = Guest.new(guest_params)
		@room = Room.find_by_id(@guest.room_id)

		if @room
			if @room.guests.size >= @room.capacity
				flash[:notice] = "room capacity not enough"	
				render 'new'
				
			else
				if @guest.save
					flash[:notice] = "guest successfully create"
					redirect_to @guest
				else
					render 'new'
				end
			end

		else
			if @guest.room_id < 0 and @guest.save
				flash[:notice] = "guest successfully create"
				redirect_to @guest
			else
				flash[:notice] = "cannot find the room"
				render 'new'
			end
		end

    #respond_to do |format|
      #if @guest.save 
       # format.html { redirect_to @guest, notice: 'Guest was successfully created.' }
        #format.json { render :show, status: :created, location: @guest }
      #else
       # format.html { render :new }
        #format.json { render json: @guest.errors, status: :unprocessable_entity }
      #end
    #end
  end

  # PATCH/PUT /guests/1
  # PATCH/PUT /guests/1.json
  def update
		@helper = Guest.new(guest_params)
		@room = Room.find_by_id(@helper.room_id)

		if @room
			if @room.guests.size >= @room.capacity
				flash[:notice] = "room capacity not enough"
				render 'edit'
			else
				if @guest.update(guest_params)
					flash[:notice] = "guest info successfully update"
					redirect_to @guest
				else
					render 'edit'
				end
			end
			
		else
			if @helper.room_id < 0 and @guest.update(guest_params)
				flash[:notice] = "guest info successfully update"
				redirect_to @guest
			else
				flash[:notice] = "cannot find the room"
				render 'edit'
			end
		end
    #respond_to do |format|
      #if @guest.update(guest_params)
        #format.html { redirect_to @guest, notice: 'Guest was successfully updated.' }
        #format.json { render :show, status: :ok, location: @guest }
      #else
        #format.html { render :edit }
        #format.json { render json: @guest.errors, status: :unprocessable_entity }
      #end
    #end
  end

  # DELETE /guests/1
  # DELETE /guests/1.json
  def destroy
    @guest.destroy
    respond_to do |format|
      format.html { redirect_to guests_url, notice: 'Guest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guest
      @guest = Guest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guest_params
      params.require(:guest).permit(:name, :email, :room_id)
    end
end
